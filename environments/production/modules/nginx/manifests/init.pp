class nginx {
    $nginx_user = 'nginx'
    notify { "Configuring nginx": }
    
    package { 'nginx': 
	ensure => 'installed',
    }
    
    file {'/usr/share/nginx/html/index.html': 
	source => 'puppet:///modules/nginx/index.html',
	mode => '0644',
    }
    
    file {'/etc/nginx/nginx.conf': 
	ensure => file,
	content => template('nginx/nginx.conf.erb'),
	notify => Service['nginx'],
    }
    
    service {'nginx': 
	ensure => 'running',
	hasrestart => true,
	hasstatus => true,
	enable => true,
    }
}
